﻿using System;

namespace Onderwerp5
{
    class Program
    {
        static void Main(string[] args)
        {
            // constructor
            BankAccount account1 = new BankAccount();
            BankAccount account2 = new BankAccount();
            Console.WriteLine(account1.Balance); 
        

            // overloading

            //private decimal balance;
            //public BankAccount()
            //{
            //balance = 0;
            //}
            //public BankAccount (int beginAmount)
            //{
            //balance= beginAmount
            //}
        
            // we hebben altijd minstens 2 constructors bij de overloading 
//_____________________________________________________

            // Static= mens 
            //  met of zonder static, de onderste code blijven hetzelfde 

            Mens m1 = new Mens();
            Mens m2 = new Mens();
            m1.Jarig();
            m1.Jarig();

            m2.Jarig();

            m1.ToonLeeftijd();
            m2.ToonLeeftijd();

            // zonder static
            // output: m1 en m2 optellen en de 2de  (we beginnen te tellen van 1) 
            // 1 +2= 3 en 2 

            // met static 
            // output: 3 jarige plus 1 want leeftijdd = 1 
            // als er nul stond zou het 3 zijn, maar de output is dus 4 

        }
    }
}

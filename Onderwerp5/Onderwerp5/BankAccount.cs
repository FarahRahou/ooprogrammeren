﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Onderwerp5
{
    class BankAccount
    {
        private decimal balance;
        public decimal Balance
        {
            get
            {
                return balance;
            }
        }
        public BankAccount() // constructor 
        {
            Console.WriteLine("Er is een nieuwe bankrekering gemaakt");
            balance = 50;
        }
    }
}

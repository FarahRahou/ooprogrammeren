﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Onderwerp5
{
    class Counter
    {
        // METHOD OVERLOAD
        public int TelOp(int getal1, int getal2, int getal3) //  Je hebt een uitgebreide versie : je hebt verschillende genres, pop en klassieke muziek
        {
            return getal1 + getal2 + getal3;
        }
        public int TelOp(int getal1, int getal2)  // Je hebt een makkelijke versie: je hebt 1 genre = pop      
        {
            return getal1 + getal2;


        // Af en toe heb je ook de andere genre nodig (klassiek)

        }
    }
}

        // onderstaande coden zullen we  niet echt nodig hebben. 

        //public int TelOp(int getal1, int getal2)
        //{
        //    return getal1 + getal2;
        //}
        //public int TelOp(double getal1, double getal2)
        //{
        //    return (int)(getal1 + getal2);
        //}

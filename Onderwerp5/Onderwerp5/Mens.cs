﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Onderwerp5
{

    
    class Mens
    {// zonder static 

        private int leeftijd = 1;
        public void Jarig()
        {
            leeftijd++;
        }
        public void ToonLeeftijd()
        {
            Console.WriteLine(leeftijd);
        }



        // met static 

        private static int leeftijdd = 1;
        public void Jarigg()
        {
            leeftijdd++;
        }
        public void ToonLeeftijdd()
        {
            Console.WriteLine(leeftijdd);
        }
    }



}

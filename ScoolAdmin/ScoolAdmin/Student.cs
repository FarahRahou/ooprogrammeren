﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Student : Person
    {

        public static List<Student> List { get; set; }
        public Student( string firstName, string lastName, DateTime birthDay, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthDay, id, schoolId)
        {
            //base: gebruik hier nu de constuctor van de ouderklasse. dus zoals class Person. je hoeft iet de datatype erbij te zetten vs kent het vanzelf.
            // tussen de accolades mag je dan bovendien zelf paramaters toevoegen die expliciet voor studenten bedoeld zijn maar dan moet je het hier ook moeten zetten zoals bijvoorbeeld
            //this.Hobby= hobby;
        }

        // type verwijderen 
        // : base => gebruik hier nu de constructor van de oude classe 
        // regel bij het gebruiken van base , als je base gebruikt voor een constructor , voor de accolade zetten

        public static string ShowAll()
        {
            string text = "Lijst van studenten:\n";
            foreach (var student in Student.List)
            {   // hier kan je geen this gebruiken want je hebt een static methode die dus geldig is voor verschillende instanties
                text += $"{student.FirstName}, {student.LastName}, {student.BirthDay}, {student.Id}, {student.SchoolId}";
            }
            return text;
        }

        public override string ShowOne()
        {
            string textStudent = $"De gegevens van student: \n {this.FirstName}, {this.LastName}, { this.BirthDay}, { this.Id}, { this.SchoolId}";
            return textStudent;
        }
    }
}


//class Student : Person
//{
//    public string FirstName { get; set; }
//    private string lastName;

//    public string LastName
//    {
//        get { return lastName; }
//        set
//        {
//            if (!string.IsNullOrEmpty(value))
//            {
//                lastName = value;
//            }
//        }
//    }
//    public DateTime Birthday { get; set; }
//    public int Id { get; set; }
//    public int SchoolId { get; set; }
//    public static List<Student> List { get; set; }
//    public string ShowOne()
//    {
//        string textStudent = "De gegevens van student: \n {this.FirstName}, {this.LastName}, { this.Birthday}, { this.Id}, { this.SchoolId}";
//        return textStudent;

//    }
//    public static string ShowAll()
//    {
//        string text = "De lijst van studenten met hun gegevens \n";
//        foreach (var student in List)
//        {  // hier kan je geen this gebruiken want je hebt een static methode die dus geldig is voor verschillende instanties
//            text += $" {student.FirstName}, {student.LastName}, geboortedatum: { student.Birthday}, studentnummer: { student.Id}, school-Id: { student.SchoolId}\n"; // niet klasse Student.SchoolId

//        }
//        return text;
//    }
//}
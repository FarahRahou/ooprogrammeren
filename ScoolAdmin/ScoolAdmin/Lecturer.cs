﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer : Person //we gaan nu adhv overerving de code korter schrijven voor lectoren en studenten door ze onder childklass te maken van parent Person
    {

        public static List<Lecturer> List { get; set; }
        public List<Course> Courses { get; set; }  //niet static want alle lectoren gaan hun EIGEN lijst moeten hebben van de vakken die ze geven

        public static string ShowAll()
        {
            string text = "Lijst van lectoren:\n";
            foreach (var lecturer in List)
            { // hier kan je geen this gebruiken want je hebt een static methode die dus geldig is voor verschillende instanties
                text += $"{lecturer.FirstName}, {lecturer.LastName}, {lecturer.BirthDay}, {lecturer.Id}, {lecturer.SchoolId}";
            }
            return text;
        }

        public Lecturer(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthday, id, schoolId, contactNumber)
        {
            //wanneer een nieuwe lector wordt aangemaakt, de lector voorzien van een lijst met zijn/haar cursussen
            //we gaan deze lijst gewoon ier maken
            this.Courses = new List<Course>();

        }
        public override string ShowOne()
        {
            string textLector =  $"Gegevens van de lector: {this.FirstName}, {this.LastName}, {this.BirthDay}, {this.Id}, {this.SchoolId}";
            return textLector;
        }

        public string ShowTaughtCourses() // de lijst laten zien
        {
           string result =  $"Vakken van deze lector: ???";
            foreach(var course in this.Courses)
            {
                result += $"{course.Title} ({course.CalculateWorkLoad()})\n";
            }
            return result;
        }
        public override string GetNameTagText()
        {
            return $" (LECTOR){this.FirstName} {this.LastName} contact: {this.ContactNumber}";
        }
    }
}
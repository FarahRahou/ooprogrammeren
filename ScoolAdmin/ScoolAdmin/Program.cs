﻿using System;
using System.Collections.Generic; // niet vergeten om list te  kunnen gebruiken


namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("************************************************");
                Console.WriteLine("Welkom bij Schoolbeheer!\n Maak een keuze uit de volgende lijst: ");
                Console.WriteLine("1. De School klasse testen");
                Console.WriteLine("2. De Student klasse testen");
                Console.WriteLine("3. De Lecturer klasse testen");
                Console.WriteLine("4. Een lijst met Person instanties maken");
                Console.WriteLine("5. De Administrative klasse gebruiken");
                Console.WriteLine("6. De associatie Lecturer en Course klasse");
                Console.WriteLine("7. De verschillende personen met naamkaartje weergeven");

                Console.WriteLine("0. Stoppen");
                Console.WriteLine("************************************************\n uw keuze: ");

                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        TestSchool();
                        break;
                    case 2:
                        TestStudent();
                        break;
                    case 3:
                        TestLecturer();
                        break;
                    case 4:
                        MakePerson();
                        break;
                    case 5:
                        TestAdministrativeStaff();
                        break;
                    case 6:
                        TestLectureCourse();
                        break;
                    case 7:
                        PersonListMetNaamkaartje();
                        break;
                    case 0:
                        done = true;
                        break;




                }


                static void TestSchool()
                {
                    Console.WriteLine("Schoolbeheer");
                    //School school1= new School(); dit is een deafault constructor. hier krijg ik nu een foutmelding want er is geen schoolconsturctor meer met 5 paramaters
                    //school1.Name = "GO! BS de Spits";
                    //school1.Street = "Thonetlaan 106";
                    //school1.PostalCode = "2050";
                    //school1.City = "Antwerpen";
                    //school1.Id = 1;

                    School school1 = new School("GO! BS de Spits", "Thonetlaan 106", "2050", "Antwerpen", 1);
                    var school2 = new School("GO! Koninklijk Atheneum Deurne", "Fr. Craeybeckxlaan 22", "2100", "Deurne", 2); //ik kan ook var gebruiken de compiler weet dat het van de type school is

                    //in de lijst weergeven MAAR EERST lijst maken met gegevens type List met school als objecten
                    List<School> schoolList = new List<School>();
                    schoolList.Add(school1);
                    schoolList.Add(school2);

                    School.List = schoolList; // de gemaakt lijst nu effectief toekennen aan de klasse School
                    Console.WriteLine(School.ShowAll());
                    Console.ReadLine();
                }

                static List<Student> TestStudent()
                {
                    ////////////////////////Studenten//////////////////////////////////
                    Student student1 = new Student("Mohamed", "El Farisi", new DateTime(1987, 12, 06), 1, 1, "0394759374");
                    Student student2 = new Student("Farah", "El Farisi", new DateTime(1987, 12, 06), 1, 4, "09039475937dss4");
                    Student student3 = new Student("Sarah", "Jansens", new DateTime(1991, 10, 21), 2, 1, "09990909090909");
                    var student4 = new Student("Bart", "Jansens", new DateTime(1990, 10, 21), 2, 3, "56868776785678");


                    //in de lijst weergeven MAAR EERST lijst maken met gegevens type List met student als objecten

                    Student.List = new List<Student>
                     {
                        student1,
                        student2,
                        student3,
                        student4
                    };

                    Console.WriteLine(Student.ShowAll()); //want return type was een string dus moet nog overgeschreven worden naaar de console
                    Console.WriteLine(student1.ShowOne());
                    return Student.List;
                }


                static List<Lecturer> TestLecturer()
                {
                    // Labo 9 en 10: 2 nieuwe lectoren aanmaken en in een list steken
                    Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");
                    Lecturer lecturer2 = new Lecturer("Anne", "Wouters", new DateTime(1968, 04, 03), 2, 2, "0489000000");
                    Lecturer.List = new List<Lecturer>
                    {
                        lecturer1,
                        lecturer2
                    };
                    Console.WriteLine(Lecturer.ShowAll());
                    Console.WriteLine(lecturer1.ShowOne());
                    return Lecturer.List;
                }


                static void TestAdministrativeStaff()
                {
                    // Labo 9 en 10: Object maken van AdministrativeStaff en toevoegen aan de lijst van personen
                    var administrativeStaff1 = new AdministrativeStaff("Raul", "Jacob", new DateTime(1985, 11, 01), 1, 1, "0489000000");
                    List<Person> persons = MakePerson();
                    persons.Add(administrativeStaff1);
                    Console.WriteLine(administrativeStaff1.ShowOne());
                    foreach (var person in persons)
                    {
                        Console.WriteLine($"{person.FirstName} {person.LastName}");
                    }

                }


                // een lijst bevolken. studenten en lecoteren hebben apart lijst maar ik wil de twee groepen samenvoegen. 
                //ik kan een lijst van type Person maken. Person class was abstract maar het idee van studenten en lectoeren zijn er wel. 
                //dus ik kan er wel een lijst van maken
                static List<Person> MakePerson()
                {
                    // Labo 9 en 10: Een lijst van Person maken met zowel studenten als lectoren in
                    // maak de lijst met studenten
                    List<Student> studentList = TestStudent();
                    // maak de lijst met lectoren
                    List<Lecturer> lecturerList = TestLecturer();
                    var persons = new List<Person>();
                    foreach (Student student in studentList)
                    {
                        persons.Add(student);
                    }

                    foreach (Lecturer lecturer in lecturerList)
                    {
                        persons.Add(lecturer);
                    }
                    return persons;
                }

                static void TestLectureCourse()
                {
                    Course theoryCourse = new TheoryCourse("OO Programmeren", 3);
                    Course aSeminar = new Seminar("Docker");
                    Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");
                    lecturer1.Courses.Add(theoryCourse);
                    lecturer1.Courses.Add(aSeminar);
                    Console.WriteLine($"Cursussen van Lector Adem: {lecturer1.ShowTaughtCourses()}");
                    Console.ReadLine();


                }

                static void PersonListMetNaamkaartje()
                {
                    // Labo 11: Voeg een protected property ContactNumber(van type string) toe aan Person en zorg
                    // dat deconstructoren dit ook aannemen; zorg dat dit getoond wordt op de naamkaartjes
                    // van personeel(Lecturer en AdministrativeStaff) maar niet van studenten.
                    Lecturer lecturer1 = new Lecturer("Adem", "Kaya", new DateTime(1976, 12, 01), 1, 1, "0489000000");
                    Student student1 = new Student("Mohamed", "El Farisi", new DateTime(1987, 12, 06), 1, 1, "0489000000");
                    var administrativeStaff1 = new AdministrativeStaff("Raul", "Jacob",
                           new DateTime(1985, 11, 01), 1, 1, "0489000000");
                    Console.WriteLine(student1.GetNameTagText());
                    Console.WriteLine(lecturer1.GetNameTagText());
                    Console.WriteLine(administrativeStaff1.GetNameTagText());
                    Console.ReadLine();
                }
            }
        }
    }
}


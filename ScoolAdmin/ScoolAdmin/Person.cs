﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Person  //abstract = onvolledige class, om te vermijden dat er instanties aangemaakt worden Person. het moet ofwel student ofwel lector zijn  
    {

        public string FirstName { get; set; }
        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }
        //birthdate zetten op public: code is niet geencapsuleerd, zelfs de klasse course kan het aanpassen. 
        //private: ook een optie maar dan,kan enkel de klasse person  birthday bereiken. dus kunnen de subklassen niets specifieks doen.
        protected DateTime BirthDay { get; set; } // behandel dit alsof het private is : protected 
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public static List<Student> List { get; set; }
        //je kan ook in een abstracte klass constructors definieren. Ik kan wel GEEN objecten maken van abstacte klassen. 
        //Maar je kan wel zeggen hoe de constructie van een persoon werkt. --> MINDER DUBBELE CODE
        protected string ContactNumber { get; set; }
        public Person(string firstName, string lastName, DateTime birthDay, int id, int schoolId, string contactNumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDay = birthDay;
            this.Id = id;
            this.SchoolId = schoolId;
            this.ContactNumber = contactNumber;
        }

        //public string ShowOne()
        //{
        //    //en persoon is niet niet nice, we verliezen gegevens van de persoon, was het een student of een lector?
        //   // string text = $"De gegevens van PERSOON : \n {this.FirstName}, {this.LastName}, { this.Birthday}, { this.Id}, { this.SchoolId}";
        //    return text;

        //} daarom doen we de volgende:
        public abstract string ShowOne(); // we moeten een showone hebben maar de werking gaat de childklassen (lector, student...) beslissen
        public virtual string GetNameTagText() //virtual > het is toegelaten om dit mechanisme aan te passen
        {
            Console.WriteLine("NAAMKAARTJES");
            return $"{this.FirstName} {this.LastName}";

        }
    }
}
    



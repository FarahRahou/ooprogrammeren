﻿using System;
using System.Collections.Generic;  // Waarom System.Collections.Generic ? 
using System.Text;                 // Zorgt er voor dat de library van list op c# komt 
                                   // c# kan dat niet van zelf weten 

namespace SchoolAdmin
{
    class AdministrativeStaff : Person
    {

        public static List<AdministrativeStaff> List { get; set; }
        public AdministrativeStaff(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthday, id, schoolId, contactNumber)
        {
            this.ContactNumber = contactNumber;
        }

        public static string ShowAll()
        {
            string text = "Lijst van administratief personeel:\n";
            foreach (var staff in AdministrativeStaff.List)
            { // hier kan je geen this gebruiken want je hebt een static methode die dus geldig is voor verschillende instanties
                text += $"{staff.FirstName}, {staff.LastName}, {staff.Id}, {staff.SchoolId}";
            }
            return text;
        }
        public override string ShowOne()
        {
            string text = $"De gegevens van administratief personeel : \n {this.FirstName}, {this.LastName}, { this.Id}, { this.SchoolId} \n";
            return text;

        }

        public override string GetNameTagText()
        {
            return $" (ADMINISTRATIE){this.FirstName} {this.LastName} contact: {this.ContactNumber}";
        }
    }
}

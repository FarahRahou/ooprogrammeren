﻿using System;
using System.Collections.Generic; 
using System.Text;

namespace SchoolAdmin
{
    // POLYMORFISME: eerst algemene idee uitgedruikt, maar het heefts specifieke verschijnselen. En elk van deze veschijnselen kunnen ook nog op een specieifieke manier werken
    //<-> abstractie: lecturers en studenten overkoepeld en daarboven Person gezet omdat het algemener was. Alles wat we zijden voor person gelde ook ver student en lectors. van onder naar bovengaan. van specifiek naar algemeen. 
    //En bij de algemene (ouderclass) kan je mss geen nieuwe objecten maken maar hbe je wel alle belangrijke zaken die je nodig hebt.
    
    // voorbeeld van polyformisme , poly betekent veel en morfe betekent vorm 
    class TheoryCourse : Course // foutmelding : deze cursus moet zelf een impementatie voorzien 
    {                           // druk op het lampje 
                                //  is een childeclass van class Course: TheoryCOurs eis een verschijningsvorm van Course.
        public byte StudyPoints { get; set; }

        public TheoryCourse(string title, byte studyPoints) : base(title)
        {
            this.StudyPoints = studyPoints;
        }
        public override uint CalculateWorkLoad()
        {
            // throw new NotImplementedException(); // het is er nog niet
            return this.StudyPoints +(uint) 4; // cast toevoegen (uint)
        } //(uint) casting: C# is niet "slim" genoeg om dat te kunnen begrijpen dus moet het erbij
    }
}

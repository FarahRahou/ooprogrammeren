﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Course // we houden enkel dingen bij die voor ons interessant zijn= abstract
    { //als we een les zetten is het altijd specifiek en geen new Course().
      //alle gemeenschappelijke  kenmmerken gaan wer hier zetten voor elke cursus
        public string Title { get; set; }

        public Course(string title) // constructor
        {
            this.Title = title;
        }

        public abstract uint CalculateWorkLoad(); // uint = alleen maar positieve getallen

    }
}

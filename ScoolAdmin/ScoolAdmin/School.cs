﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
        // PROPERTIES
        //we gaan een fulprop voor naam gebruiken want de naam mag niet leeg zijn. setter gaat contorleren of dat het leeg is of niet
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrEmpty(name))
                {
                    name = value;
                }

            }
        }
        // de rest mogen gewone properties zijn
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public int Id { get; set; }
        public static List<School> List { get; set; }

        public School(string name, string street, string postalCode, string city, int id) // ik wil geen default constructor
        {
            //nu gaan we al de properties instellen. in program.cs stelde ik de properties in NA dat ik de object heb gemaakt. Buiten het object.hier doe ik het direct
            this.Name = name;
            this.Street = street;
            this.PostalCode = postalCode;
            this.City = city;
            this.Id = id;
        }

        // METHODEN
        public static string ShowAll()
        {
            string text = "Lijst van scholen: \n";
            foreach (var school in List) // zie public static List
            {
                text += $"{school.Name}, {school.Street}, {school.City}, {school.Id};\n";
            }
            return text;
        }
        public string ShowOne()
        {
            string text = $"Gegevens van de school: {this.Name}, {this.Street}, {this.City}, {this.Id}.";
            return text;
        }

    }
}




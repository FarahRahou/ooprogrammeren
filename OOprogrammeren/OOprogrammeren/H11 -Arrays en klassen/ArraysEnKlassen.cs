﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArraysEnKlassen
    {
        public static void AskForPrices()
        {
            double[] prices = new double[20];
            Console.WriteLine("Gelieve 20 prijzen in te geven.");
            for (int i = 0; i < prices.Length; i++)
            {
                prices[i] = Convert.ToDouble(Console.ReadLine());
            }
            double sum = 0;
            foreach(double price in prices)
            {
                if (price >= 5)
                {
                    Console.WriteLine($"{price: F2}");
                }
                sum += price;
            }
            Console.WriteLine($"Het gemiddelde bedrag is: {sum / prices.Length:F2}");
        }
        public static void startSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1.H11.Prijzen");
            Console.WriteLine("2.h11-Speelkaarten");
            Console.WriteLine("3.H11-Studentklasse");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Prices.main();
                    break;
                case 2:
                    Speelkaart.ShowDeck();
                    break;
                case 3:
                    Studentt.ExecuteStudentMenu();
                    break;
                default:
                    Console.WriteLine("Ongeldige keuze!");
                    break;
            }
        }
    }
}

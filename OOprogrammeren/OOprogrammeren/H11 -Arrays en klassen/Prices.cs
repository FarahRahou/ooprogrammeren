﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Prices
    {
        public static void main()
        {
            AskForPrices();
        }
        static void AskForPrices()
        {
            double[] array = new double[5];
            Console.WriteLine("Gelieve 20 prijzen in te geven.");

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = Convert.ToDouble(Console.ReadLine());
            }

            double sum = 0;
            foreach (double numbers in array)
            {
                sum += numbers;
                if (numbers >= 5)
                {
                    Console.WriteLine(numbers);
                }
            }
            Console.WriteLine($"Het gemiddelde bedrag is {sum / array.Length}.");
        }
    }
}

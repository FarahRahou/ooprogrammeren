﻿using System;
using System.Collections.Generic;
using System.Text;


namespace OOP
{
    class Speelkaart
    {
        private int number;
        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                if (value >= 1 && value <= 13)
                    number = value;
            }
        }

        public Suites Suite { get; set; }

        public static List<Speelkaart> GenerateDeck()
        {
            List<Speelkaart> deck = new List<Speelkaart>();

            foreach (Suites suite in Enum.GetValues(typeof(Suites)))
            {

                for (int i = 1; i <= 13; i++)
                {
                    Speelkaart card = new Speelkaart
                    {
                        Number = i,
                        Suite = suite
                    };

                    deck.Add(card);

                }

            }
            return deck;
        }
        public static void ShowDeck()
        {
            foreach (Speelkaart card in GenerateDeck())
            {
                Console.WriteLine(card.Number + " " + card.Suite.ToString());
            }
        }

        public static void ShowShuffledDeck(List<Speelkaart> cards)
        {
            int initialCount = cards.Count;
            Random rng = new Random();

            for (int i = 0; i < initialCount; i++)
            {
                int r = rng.Next(cards.Count);
                Console.WriteLine(cards[r].Number + " " + cards[r].Suite);
                cards.RemoveAt(r);
            }
        }

    }

}

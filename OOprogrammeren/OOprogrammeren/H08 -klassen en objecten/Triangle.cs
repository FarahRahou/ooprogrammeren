﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP // ALTIJD DEZELFDE NAMENSPACE NAAM 
{

        class Triangle
        {
        private double basis = 1.0;

            public double Basis
            {
                get
                {
                    return basis;
                }
                set
                {
                if(value > 0)
                {
                    basis = value;
                }
                 
                }
            }
        private double height;
        public double Height
        {

            get
            {
                return height; // altijd een kleine letter, achter de return
            }

            set
            {
                if (value > 0)
                {
                    height = value;
                }
                else
                {
                    Console.WriteLine($"Het is verboden een breedte van {value} in te stellen!");
                }
            }
        }

            public double Surface
        {
            get
            {
                return (height * basis) / 2;

            }
        }
            public double AreaTriangle()
        {
            return height * basis / 2;
        }

        }
        
    }


           


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class StudentKlasse

    {

        public string Name // auto property 
        {
            get;
            set;
        }
        private byte age;
        public byte Age // full property
        {
            get
            {
                return age;
            }

            set
            {
                if (age >= 0 && value <= 120)
                {
                    age = value;
                }
            }
        }
        public ClassGroups ClassGroup
        {
            get; // opvragen
            set; // instellen 
        }

        // als je 'propfull" schrijft en dan tap tap dan krijg je al een full property
        private byte markCommunication;

        public byte MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (value >= 0 && value <= 20)
                    markCommunication = value;
            }
        }

        //___________________________________________________________________________________________

        private byte markWebTech;

        public byte MarkWebTech
        {
            get { return markWebTech; }
            set
            {
                if (value >= 0 && value <= 20)
                    markWebTech = value;
            }
        }

        //__________________________________________________________________________________________

        private int markProgrammingPrinciples;

        public int MarkProgrammingPrinciples
        {
            get { return markProgrammingPrinciples; }

            set { markProgrammingPrinciples = value; }
        }

        //____________________________________________________________________________________________

        public double OveralMark
        {
            get
            {
                return (MarkCommunication + MarkProgrammingPrinciples + MarkWebTech) / 3.0;
            }
        }

        public void ShowOverview()
        {
            Console.WriteLine($"{Name}, {Age} jaar");
            Console.WriteLine($"Klas: {ClassGroup}");
            Console.WriteLine();
            Console.WriteLine("Cijferrapport");
            Console.WriteLine("*************");
            Console.WriteLine($"Programming Principles:\t\t{MarkProgrammingPrinciples}");
            Console.WriteLine($"Webtechnologie:\t\t{MarkWebTech}");
            Console.WriteLine($"Gemiddelde:\t\t{OveralMark:F1}");

        }

        public static void main() // niet gekoppeld aan een specifieke student= daarom een static, hoort niet bij een bepaald object. Gaat over de hele klasse
        {
            StudentKlasse student1 = new StudentKlasse();
            student1.ClassGroup = ClassGroups.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            student1.ShowOverview();
        }
    }
}
// Maak een nieuwe klasse Student. Deze klasse heeft 6 properties. Leeftijd en de punten stel je voor met full properties.
//Een student kan nooit ouder zijn dan 120. Je kan ook nooit een cijfer boven 20 behalen. Over leeftijden en cijfers onder 0 
//hoef je je geen zorgen te maken, want de achterliggende variabelen zijn bytes en die zijn altijd minstens 0.

//Name(string)
//Age(byte)
//ClassGroup(maak hiervoor een enum ClassGroups)
//MarkCommunication(byte)
//MarkProgrammingPrinciples(byte)
//MarkWebTech(byte)
//Voeg aan de klasse een read-only property OverallMark toe.Deze berekent het gemiddelde van de 3 punten als double.
//Voeg aan de klasse ook de methode ShowOverview() toe. Deze methode zal een volledig rapport van de student tonen 
//(inclusief het gemiddelde m.b.v.de OverallMark-property).
//Test je programma door enkele studenten aan te maken en in te stellen.Volgende statische methode Main zet je in de klasse Student.

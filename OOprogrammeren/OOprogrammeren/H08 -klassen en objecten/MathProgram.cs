﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MathProgram
    {
        public static void main()
        {
            Console.WriteLine(DoubleAddTwo(13));
        }
        public static int DoubleAddTwo(int n)
        {
            return 2 * n + 2;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class KlassenEnObjecten
    {
        public static void startSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1.H8-RapportModule-V1");
            Console.WriteLine("2.H8-RapportModule-V2");
            Console.WriteLine("3.H8-Getallencombinatie");
            Console.WriteLine("4.H8-Figuren");
            Console.WriteLine("5.H8-Studentklasse");
            Console.WriteLine("6.H8-uniform-soldiers");
            Console.WriteLine("7.H8-utility-methode");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    ResultV1.main();
                    break;
                case 2:
                    ResultV2.main();
                    break;
                case 3:
                    NumberCombination.main();
                    break;
                case 4:
                    FigurenProgramma.main();
                    break;
                case 5:
                    StudentKlasse.main();
                    break;
                case 6:
                    SoldierGame.main();
                    break;
                case 7:
                    MathProgram.main();
                    break;
                default:
                    Console.WriteLine("Ongeldige keuze!");
                    break;
            }
        }
    }
}

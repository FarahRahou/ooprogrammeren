﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
        class Rectangle
        {
            private double width = 1.0;
            public double Width
            {
                get
                {
                return width;
                }

                set
                {
                    if (value > 0)
                    {
                        width = value;
                    }
                    else
                    {
                        Console.WriteLine($"Het is verboden een breedte van {value} in te stellen!");
                    }
                }

            }

            private double height = 1.0;
            public double Height
            {

                get
                {
                    return height; // altijd een kleine letter, achter de return
                }

                set
                {
                    if (value > 0)
                    {
                        height = value;
                    }
                    else
                    {
                        Console.WriteLine($"Het is verboden een breedte van {value} in te stellen!");
                    }
                }

            }
            public double Surface
            {

                get
                {
                    return width * height;
                }
            }
        }
}
// sws een full property omdat bij een auto property het niet kan controleren


// Er is een klasse Rectangle met properties Width en Height en een klasse Triangle met Base en Height. 
//Je programma maakt de figuren die hierboven beschreven worden aan met waarde 1.0 voor elke afmeting en stelt daarna hun afmetingen in 
//via de setters voor deze properties. De oppervlakte wordt bepaald in een read-only property, Surface van het type double.
//Indien om het even welk van deze properties wordt ingesteld op 0 (of minder) wordt er een foutboodschap afgeprint 
//en wordt de afmeting niet aangepast.
//De wiskundige formule voor de oppervlakte van een driehoek is basis* hoogte / 2.
//Schrijf de voorbeelden uit in de Main van een extra klasse, FigureProgram.


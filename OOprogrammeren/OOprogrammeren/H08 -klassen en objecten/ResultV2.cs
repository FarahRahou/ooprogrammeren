﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV2
    {
        public byte Percentage
        {
            get;
            set;
        }

        public Honors ComputeHonors()
        {
            if (Percentage < 50)
            {
                return Honors.NietGeslaagd;
            }

            else if (Percentage <= 68)
            {
                return Honors.Voldoende;
            }

            else if (Percentage <= 75)
            {
                return Honors.Onderscheiding;
            }

            else if (Percentage <= 85)
            {
                return Honors.GroteOnderscheiding;
            }

            else
            {
                return Honors.GrootsteOnderscheiding;
            }


        }
        public static void main()
        {
            ResultV2 result1 = new ResultV2();
            result1.Percentage = 40;
            Console.WriteLine(result1.ComputeHonors());

            ResultV2 result2 = new ResultV2();
            result2.Percentage = 60;
            Console.WriteLine(result2.ComputeHonors());

            ResultV2 result3 = new ResultV2();
            result3.Percentage = 70;
            Console.WriteLine(result3.ComputeHonors());

            ResultV2 result4 = new ResultV2();
            result4.Percentage = 80;
            Console.WriteLine(result4.ComputeHonors());

            ResultV2 result5 = new ResultV2();
            result5.Percentage = 90;
            Console.WriteLine(result5.ComputeHonors());
        }
    }
}

//Ontwerp een klasse ResultV2 die je zal vertellen wat je graad is gegeven een bepaald behaald percentage.
//Het enige dat je aan een ResultV2-object moet kunnen geven is het behaalde percentage.Enkel het totaal behaalde percentage wordt bijgehouden.
//Via een methode ComputeHonors kan de behaalde graad worden teruggegeven.Dit werkt op dezelfde manier als in versie 1 van deze oefening,
//maar de verschillende graden worden voorgesteld met een Enum, Honors.De methode ComputeHonors toont het resultaat niet,
//maar geeft een waarde van deze Enum terug. Het is aan de Main om deze waarde af te printen, zodat je kan zien of je code werkt.
//Test je klasse op dezelfde manier als versie 1. De teruggegeven waarden van Honors mag je in de Main meegeven aan Console.WriteLine.


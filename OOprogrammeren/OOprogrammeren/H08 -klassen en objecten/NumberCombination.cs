﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class NumberCombination
    {
        public static void main()
        {
            NumberCombination pair1 = new NumberCombination();
            pair1.Number1 = 12;
            pair1.Number2 = 34;
            Console.WriteLine("Paar:" + pair1.Number1 + ", " + pair1.Number2);
            Console.WriteLine("Sum = " + pair1.Sum());
            Console.WriteLine("Verschil = " + pair1.Difference());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());
        }


        public int Number1
        {
            get;
            set;
        }

        public int Number2
        {
            get;
            set;
        }

        public double Sum()
        {
            return Number1 + Number2;
        }

        public double Difference()
        {
            return Number1 - Number2;
        }

        public double Product()
        {
            return Number1 * Number2;
        }

        public double Quotient()
        {
            return Number1 / Number2;

            if (Number2 != 0)
            {
                return Number1 / Number2;
            }

            else
            {
                Console.WriteLine("Error");
                return Number1 / Number2;
            }
        }
    }
}

// Maak een eenvoudige klasse NumberCombination. Deze klasse bevat 2 getallen (type int). Er zijn 4 methoden, die allemaal een double teruggeven:
//Sum: geeft som van beide getallen weer
//Difference: geeft verschil van beide getallen weer
//Product: geeft product van beide getallen weer
//Quotient: geeft deling van beide getallen weer.Print "Error" naar de console indien je zou moeten delen door 0 en voer dan de deling uit.
//Wat er dan gebeurt, is niet belangrijk.
//Gebruik full properties voor Number1 en Number2 en toon in je Main aan dat je code werkt.


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Soldier
    {
        private int health;
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    health = value;
                }
            }
        }
        private static int damage = 20;
        public static int Damage
        {
            get
            {
                return damage;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    damage = value;
                }
            }
        }

    }
}

// Gebruik hiervoor een sleutelwoordje dat zorgt dat de hoeveelheid schade niet specifiek is voor één soldaat,
//maar dezelfde is voor alle soldaten. We hebben het ook gebruikt om te bepalen of een jaar een schrikkeljaar is,
//omdat die berekening niet specifiek is voor één datum. Pas ook de demonstratiemethode aan.

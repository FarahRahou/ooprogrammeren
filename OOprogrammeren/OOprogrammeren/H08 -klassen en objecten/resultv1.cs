﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV1
    {
        public byte Percentage
        {
            get;
            set;
        }

        public void PrintHonors()
        {
            if (Percentage < 50)
            {
                Console.WriteLine("Niet geslaagd");
            }

            else if (Percentage <= 68)
            {
                Console.WriteLine("Voldoende");
            }

            else if (Percentage <= 75)
            {
                Console.WriteLine("Onderscheiding");
            }

            else if (Percentage <= 85)
            {
                Console.WriteLine("Grote onderscheiding");
            }

            else
            {
                Console.WriteLine("Grootste onderscheiding");
            }


        }
        public static void main()
        {
            ResultV1 result1 = new ResultV1();
            result1.Percentage = 40;
            result1.PrintHonors();

            ResultV1 result2 = new ResultV1();
            result2.Percentage = 60;
            result2.PrintHonors();

            ResultV1 result3 = new ResultV1();
            result3.Percentage = 70;
            result3.PrintHonors();

            ResultV1 result4 = new ResultV1();
            result4.Percentage = 80;
            result4.PrintHonors();

            ResultV1 result5 = new ResultV1();
            result5.Percentage = 90;
            result5.PrintHonors();
        }
    }
}

// Ontwerp een klasse ResultV1 die je zal tonen wat je graad is gegeven een bepaald behaald percentage. 
//Het enige dat je aan een ResultV1-object moet kunnen geven is het behaalde percentage. Enkel het totaal behaalde percentage wordt bijgehouden.
//Via een methode PrintHonors kan de behaalde graad worden weergegeven. Dit zijn de mogelijkheden:

//< 50: niet geslaagd;
//tussen 50 en 68: voldoende;
//tussen 68 en 75: onderscheiding;
//tussen 75 en 85: grote onderscheiding;
//> 85: grootste onderscheiding.

//Je hoeft voorlopig geen rekening te houden met ongeldige waarden.Test je klasse door ResultV1 ook van een statische methode Main te voorzien,
// waarin je enkele objecten van deze klasse aanmaakt en de verschillende properties waarden te geeft en vervolgens PrintHonors oproept.


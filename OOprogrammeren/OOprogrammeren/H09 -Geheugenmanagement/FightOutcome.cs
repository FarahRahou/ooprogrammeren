﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum FightOutcome
    {
        WIN, 
        LOSS,
        UNDECIDED
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum PokeSpecies
    {
        Bulbasaur, 
        Charmander, 
        Squirtle, 
        Pikachu
    }
}

// maak hiervoor een enum PokeSpecies met waarden Bulbasaur, Charmander, Squirtle, Pikachu
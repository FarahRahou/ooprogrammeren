﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Pokemon
    {
        public static void main()
        {

        }
        private int maxHP;
        public int MaxHP
        {
            get
            {
                return maxHP;
            }
            set
            {
                if (value < 1)
                {
                    maxHP = 1;
                }
                else if (value >= 1 && value <= 1000)
                {
                    maxHP = value;
                }
                else
                {
                    maxHP = 1000;
                }
            }
        }
        private int hp;
        public int HP
        {

            get
            {
                return hp;
            }
            set
            {
                if (value < 0)
                {
                    hp = 0;
                }
                else if (value >= MaxHP)
                {
                    hp = MaxHP;
                }
                else
                {
                    hp = value;
                }
            }
        }
        public PokeSpecies PokeSpecies
        {
            get;
            set;
        }
        public PokeTypes PokeTypes
        {
            get;
            set;
        }
        //h10-pokeconstructie
        public Pokemon()
        {

        }
        public Pokemon(int maxHP, int hp, PokeTypes pokeType, PokeSpecies pokeSpecies)
        {
            this.MaxHP = maxHP;
            this.HP = hp;
            this.PokeTypes = pokeType;
            this.PokeSpecies = pokeSpecies;
        }
        //(h10-chaining)
        public Pokemon(int maxHP, PokeTypes pokeType, PokeSpecies pokeSpecies) : this(maxHP, maxHP / 2, pokeType, pokeSpecies)
        {

        }
        private static int tellerGrass, tellerFire, tellerWater, tellerElectric = 0;
        public static void ConstructPokemonChained()
        {
            Pokemon pokemon1 = new Pokemon(40, 20, PokeTypes.Electric, PokeSpecies.Pikachu);
            Console.WriteLine($"De nieuwe {pokemon1.PokeSpecies} heeft maximum {pokemon1.MaxHP} HP en heeft momenteel {pokemon1.HP} HP.");
        }
        public static void DemonstrateCounter()
        {
            Random ranGen = new Random();
            for (int i = 0; i < 5; i++)
            {
                PokeTypes randomTypes;
                PokeSpecies randomSpecies;
                int randomNumber = ranGen.Next(1, 5);
                if (randomNumber == 1)
                {
                    randomTypes = PokeTypes.Grass;
                    randomSpecies = PokeSpecies.Bulbasaur;
                }
                else if (randomNumber == 2)
                {
                    randomTypes = PokeTypes.Fire;
                    randomSpecies = PokeSpecies.Charmander;
                }
                else if (randomNumber == 3)
                {
                    randomTypes = PokeTypes.Water;
                    randomSpecies = PokeSpecies.Squirtle;
                }
                else
                {
                    randomTypes = PokeTypes.Electric;
                    randomSpecies = PokeSpecies.Pikachu;
                }
                Pokemon myPokemon = new Pokemon(20, randomTypes, randomSpecies);
                for (int j = 0; j < ranGen.Next(5, 11); j++)
                {
                    myPokemon.Attack();
                }
            }
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Grass`:{Pokemon.tellerGrass}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Fire`:{Pokemon.tellerFire}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Water`:{Pokemon.tellerWater}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Electric`:{Pokemon.tellerElectric}");
        }
        public void Attack()
        {
            if (PokeTypes == PokeTypes.Grass)
            {
                Pokemon.tellerGrass++;
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else if (PokeTypes == PokeTypes.Fire)
            {
                Pokemon.tellerFire++;
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (PokeTypes == PokeTypes.Water)
            {
                Pokemon.tellerWater++;
                Console.ForegroundColor = ConsoleColor.Blue;
            }
            else if (PokeTypes == PokeTypes.Electric)
            {
                Pokemon.tellerElectric++;
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            Console.WriteLine($"{PokeSpecies.ToString().ToUpper()}");
            Console.ResetColor();
        }
        public static void MakePokemon()
        {
            Pokemon bulbasaur = new Pokemon(20, 20, PokeTypes.Grass, PokeSpecies.Bulbasaur);
            Pokemon charmander = new Pokemon(20, 20, PokeTypes.Fire, PokeSpecies.Charmander);
            Pokemon squirtle = new Pokemon(20, 20, PokeTypes.Water, PokeSpecies.Squirtle);
            Pokemon pikachu = new Pokemon(20, 20, PokeTypes.Electric, PokeSpecies.Pikachu);
            bulbasaur.Attack();
            charmander.Attack();
            squirtle.Attack();
            pikachu.Attack();
        }


        public static Pokemon FirstConsciousPokemon(Pokemon[] pokemons)
        {
            foreach (Pokemon somepokemon in pokemons)
            {
                if (somepokemon.HP > 0)
                {
                    return somepokemon;
                }
            }
            return null;
        }
        public static void TestConsciousPokemon()
        {
            Pokemon bulbasaur = new Pokemon(20, 0, PokeTypes.Grass, PokeSpecies.Bulbasaur);
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.HP = 0;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeTypes = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.HP = 2;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeTypes = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.HP = 20;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeTypes = PokeTypes.Electric;
            Pokemon[] Pokemons = { bulbasaur, charmander, squirtle, pikachu };
            FirstConsciousPokemon(Pokemons).Attack();
        }
        public static void TestConsciousPokemonSafe()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeTypes = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.HP = 0;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeTypes = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.HP = 0;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeTypes = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.HP = 0;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeTypes = PokeTypes.Electric;
            Pokemon[] Pokemons = { bulbasaur, charmander, squirtle, pikachu };
            if (FirstConsciousPokemon(Pokemons) == null)
            {
                Console.WriteLine("al je Pokemons zijn KO! Haastje naar het pokecenter");
            }
            else
            {
                FirstConsciousPokemon(Pokemons).Attack();
            }


        }
        public static void RestoreHP(Pokemon pokemon)
        {
            pokemon.HP = pokemon.MaxHP;

        }
        public static void DemoRestorePokemon()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeTypes = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.HP = 0;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeTypes = PokeTypes.Fire;
            Pokemon squirtle = new Pokemon();
            squirtle.MaxHP = 20;
            squirtle.HP = 0;
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeTypes = PokeTypes.Water;
            Pokemon pikachu = new Pokemon();
            pikachu.MaxHP = 20;
            pikachu.HP = 0;
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeTypes = PokeTypes.Electric;
            Pokemon[] Pokemons = { bulbasaur, charmander, squirtle, pikachu };
            // aanmaken van array bewusteloze Pokemon van 4 soorten zoals eerder: zelf doen
            for (int i = 0; i < Pokemons.Length; i++)
            {
                Pokemon.RestoreHP(Pokemons[i]);
            }

            for (int i = 0; i < Pokemons.Length; i++)
            {
                Console.WriteLine(Pokemons[i].HP);
            }
        }
        public FightOutcome fightOutcomes
        {
            get;
            set;
        }
        public static FightOutcome FightOutcome(Pokemon pokemon1, Pokemon pokemon2, Random ranGen)
        {
            Pokemon[] pokemons = { pokemon1, pokemon2 };
            while (pokemon1.HP > 0 && pokemon2.HP > 0)
            {
                int index = ranGen.Next(0, 2);
                pokemons[index].Attack();
                pokemons[(index + 1) % 2].HP -= ranGen.Next(0, 21);
                if (pokemons[(index + 1) % 2].HP > 0)
                {
                    pokemons[(index + 1) % 2].Attack();
                    pokemons[index].HP -= ranGen.Next(0, 21);
                }
                //else
            }
            if (pokemon1.HP == 0)
            {
                return OOP.FightOutcome.LOSS;
            }
            else
            {
                return OOP.FightOutcome.WIN;
            }
        }
        public static void DemoFightOutcome()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeTypes = PokeTypes.Grass;
            Pokemon charmander = new Pokemon();
            charmander.MaxHP = 20;
            charmander.HP = 0;
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeTypes = PokeTypes.Fire;
            Random ranGen = new Random();
            Console.WriteLine(FightOutcome(bulbasaur, charmander, ranGen));
        }




    }

}

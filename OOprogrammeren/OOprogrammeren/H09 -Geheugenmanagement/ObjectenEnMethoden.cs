﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ObjectenEnMethoden
    {
        public static void startSubmenu()
        {


            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1.PokeAttack");
            Console.WriteLine("2.consciouspokemon");
            Console.WriteLine("3.consciouspokemon-improved");
            Console.WriteLine("4.pokevalueref");
            Console.WriteLine("5.fight");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pokemon.MakePokemon();
                    break;
                case 2:
                    Pokemon.TestConsciousPokemon();
                    break;
                case 3:
                    Pokemon.TestConsciousPokemonSafe();
                    break;
                case 4:
                    Pokemon.DemoRestorePokemon();
                    break;
                case 5:
                    Pokemon.DemoFightOutcome();
                    break;
                default:
                    Console.WriteLine("Ongeldige keuze!");
                    break;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeavanceerdeKlassenEnObjecten
    {
        public static void startSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1.Chaining");
            Console.WriteLine("2.DemonstrateCounter");
            Console.WriteLine("3.Raffle");
            Console.WriteLine("4.CSV-file");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pokemon.ConstructPokemonChained();
                    break;
                case 2:
                    Pokemon.DemonstrateCounter();
                    break;
                case 3:
                    Ticket.Raffle();
                    break;
                case 4:
                    CSVDemo.Run();
                    break;
                default:
                    Console.WriteLine("Ongeldige keuze!");
                    break;
            }
        }
    }
}


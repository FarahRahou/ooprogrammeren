﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticket
    {
        public byte Prize
        {
            get;
            set;
        }
        /*
         * private byte prize;
         * public byte Prize
         * get
         * {
         * return this.prize;
         * }
         * private set{
         * if(value >=1 && value <=100){
         * this.prize = value
         * }}
         */
        private static Random ranGen = new Random();
        public Ticket()
        {
            Prize = Convert.ToByte(ranGen.Next(1, 101));
        }
        public static void Raffle()
        {
            for (int i = 0; i < 10; i++)
            {
                Ticket myTicket = new Ticket();
                Console.WriteLine($"Waarde van het lotje:{myTicket.Prize}");
            }
        }
    }
}


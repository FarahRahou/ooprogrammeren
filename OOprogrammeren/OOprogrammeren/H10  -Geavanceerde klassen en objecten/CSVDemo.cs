﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace OOP
{
    static class CSVDemo
    {

        public static void Run()
        {
            WebClient wc = new WebClient();
            string fileText = wc.DownloadString("http://samplecsvs.s3.amazonaws.com/SalesJan2009.csv");
            string[] lines = fileText.Split("\r");
            for (int i = 0; i < lines.Length; i++)
            {
                string[] fields = lines[i].Split(",");
                Console.WriteLine($"{fields[0]}{fields[1]}{fields[2]}");
            }
        }
    }
}

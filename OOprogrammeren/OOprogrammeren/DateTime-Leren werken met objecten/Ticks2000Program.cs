﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    class Ticks2000Program
    {
        public static void Main()
        {
            DateTime centuryBegin = new DateTime(2000, 1, 1);
            DateTime currentDate = DateTime.Now;

            TimeSpan difference = currentDate - centuryBegin;
            CultureInfo belgianCI = new CultureInfo("nl-BE");

            Console.WriteLine($"Sinds 1 januari 2000 zijn er {difference.Ticks} ticks voorbijgegaan.");
        }
    }
}

// .NET stelt deze fracties (1 / 10000 milliseconden) voor als "ticks"
// We willen weten hoe veel ticks er voorbijgegaan zijn sinds het absolute begin van het jaar 2000
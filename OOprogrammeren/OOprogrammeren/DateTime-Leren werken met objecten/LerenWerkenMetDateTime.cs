﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LerenWerkenMetDateTime
    {
        public static void StartSubmenu()
        {


            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. Datum invoeren = dan krijg je de volledige dag in letters (H8-dag-van-de-week)\n" +
                              "2. Aantal ticks sinds 2000? (H8-ticks-sinds-2000)\n" +
                              "3. Aantal schrikkeljaren tussen 1800 en 2020? (H8-schrikkelteller)\n" +
                              "4. Hoelang duurt het om een array van 1 miljoen te maken? (H8-simpele-timing)\n");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    LerenWerkenMetDateTime.StartSubmenu();
                    break;
                case 2:
                    Ticks2000Program.Main();
                    break;
                case 3:
                    LeapYearProgram.Main();
                    break;
                case 4:
                    ArrayTimerProgram.Main();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze!");
                    break;
            }
        }
    }
}

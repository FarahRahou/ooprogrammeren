﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LeapYearProgram
    {
        public static void Main()
        {
            int counter = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    counter++;
                }
            }
            Console.WriteLine($"Er zijn {counter} schrikkeljaren tussen 1800 en 2020.");
        }
    }
}

// implementeer zelf een logica voor schrikkeljaren, maar laat dit over aan de klassen DateTime
// maak gebruik van een statische methode van deze klasse
// noem je klasse LeapYearProgram en voorzie ze van een Main


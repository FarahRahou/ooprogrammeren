﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    class DayOfWeekProgram
    {
        public static void Main() // schrijf je code in de statische methode Main
        {
            Console.WriteLine("Welke dag?");
            int day = int.Parse(Console.ReadLine());

            Console.WriteLine("Welke maand?");
            int month = int.Parse(Console.ReadLine());

            Console.WriteLine("Welk jaar?");
            int year = int.Parse(Console.ReadLine());

            DateTime datetime = new DateTime(year, month, day);
            CultureInfo belgianCI = new CultureInfo("nl-BE");
            Console.WriteLine($"{datetime.ToString("dd MMMM yyyy", belgianCI)} is een {datetime.ToString("dddd", belgianCI)}");

            // De .NET klasse DateTime is de ideale manier om te leren werken met objecten. 
            //Het is een nuttige en toegankelijk klasse
        }
    }       // yyyy en dd is in kleine letters omdat het in getalle wordt geschreven          
}           // MMMM is in grote letter omdat je de maand lang uit schrijft 


// OPDRACHT

// je moet eerst de dag, maand en jaar opvragen en een DateTime aanmaken
// daarna moet je laten zien over welke dag van de week het gaat
// gebruik hiervoor formattering van een DateTime
// laat ook de datum zelf zien in een formaat dat leesbaar is voor de gebruiker
// als je computer niet volledig ingesteld is op Belgisch Nederlands, kan het resultaat er wat anders uitzien.
// noem de klasse waarin je dit schrijft DayOfWeekProgram
// schrijf je code in de statische methode Main
// roep de statische Main van DayOfWeekProgram op in het keuzemenu van Program

